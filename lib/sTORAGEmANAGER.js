(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("sTORAGEmANAGER", [], factory);
	else if(typeof exports === 'object')
		exports["sTORAGEmANAGER"] = factory();
	else
		root["sTORAGEmANAGER"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	var _localStorage = __webpack_require__(1);
	
	var _localStorage2 = _interopRequireDefault(_localStorage);
	
	var _parseRestApi = __webpack_require__(2);
	
	var _parseRestApi2 = _interopRequireDefault(_parseRestApi);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	var _adapters = {
	  'local-storage': _localStorage2.default,
	  'parse-rest-api': _parseRestApi2.default
	};
	
	var StorageManager = function StorageManager(config) {
	  _classCallCheck(this, StorageManager);
	
	  if (!config) {
	    throw new Error('[StorageManager]: expect an adapter config object as an argument!');
	  } else if ((typeof config === 'undefined' ? 'undefined' : _typeof(config)) === 'object' && !Array.isArray(config)) {
	    if (_adapters[config.name]) {
	      var adapter = _adapters[config.name](config);
	      return Object.assign(this, adapter);
	    } else {
	      throw new Error('[StorageManager]: invalid adapter name!');
	    }
	  } else {
	    throw new Error('[StorageManager]: adapter config must be an object!');
	  }
	};
	
	exports.default = StorageManager;
	module.exports = exports['default'];

/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * LocalStorage adapter
	 */
	
	exports.default = function (config) {
	  return {
	    /**
	     * Adapter name
	     */
	    name: config.name,
	    /**
	     * Retrieve record
	     * @param  {mODELcLASS} modelClass
	     * @param  {String} id
	     * @return {Promise}
	     */
	    retrieve: function retrieve(modelClass, id) {
	      return new Promise(function (resolve, reject) {
	        var records = localStorage.getItem(modelClass.name) || '{}';
	        var record = JSON.parse(records)[id];
	
	        if (record) {
	          resolve(record);
	        } else {
	          reject('There is no such record of the type "' + modelClass.name + '" with the id "' + id + '"');
	        }
	      });
	    },
	
	    /**
	     * Retrive all records
	     * @param  {mODELcLASS} modelClass
	     * @return {Promise}
	     */
	    retrieveAll: function retrieveAll(modelClass) {
	      return new Promise(function (resolve, reject) {
	        try {
	          var records = JSON.parse(localStorage.getItem(modelClass.name) || '{}');
	
	          if (records) {
	            resolve(records);
	          } else {
	            reject('There is no "' + modelClass.name + '" object class');
	          }
	        } catch (e) {
	          reject(e);
	        }
	      });
	    },
	
	    /**
	     * Add record
	     * @param  {mODELcLASS} modelClass
	     * @param  {Object} record
	     * @return {Promise}
	     */
	    add: function add(modelClass, record) {
	      var _this = this;
	
	      return new Promise(function (resolve, reject) {
	        var id = record[modelClass.standardId];
	
	        if (!id) {
	          throw new Error('[sTORAGEmANAGER]: The record doesn\'t contain standard id!');
	        }
	
	        _this.retrieve(modelClass, id).then(function (result) {
	          reject('The record with the given id "' + id + '" already exists.');
	        }, function () {
	          var storageName = modelClass.name;
	          var records = JSON.parse(localStorage.getItem(storageName) || '{}');
	
	          records[id] = record;
	          localStorage.setItem(storageName, JSON.stringify(records));
	
	          modelClass.instances = modelClass.create(record); // ??? why we need to do this
	
	          resolve(records);
	        });
	      });
	    },
	
	    /**
	     * Update record
	     * @param  {mODELcLASS} modelClass
	     * @param  {String} id
	     * @param  {Object} record
	     * @return {Promise}
	     */
	    update: function update(modelClass, id, record) {
	      return new Promise(function (resolve, reject) {
	        try {
	          var storageName = modelClass.name;
	          var _id = record[record[_id]];
	          var records = JSON.parse(localStorage.getItem(storageName) || '{}');
	
	          records[_id] = record;
	          localStorage.setItem(storageName, JSON.stringify(records));
	
	          modelClass.instances = modelClass.create(record); // ??? why we need to do this
	
	          resolve(records);
	        } catch (e) {
	          reject(e);
	        }
	      });
	    },
	
	    /**
	     * Destroy record
	     * @param  {mODELcLASS} modelClass
	     * @param  {String} id
	     * @return {Promise}
	     */
	    destroy: function destroy(modelClass, id) {
	      var _this2 = this;
	
	      return new Promise(function (resolve, reject) {
	        _this2.retrieve(modelClass, id).then(function (result) {
	          var storageName = modelClass.name;
	          var records = JSON.parse(localStorage.getItem(storageName) || '{}');
	
	          delete records[id];
	          delete modelClass.instances[id];
	
	          localStorage.setItem(storageName, JSON.stringify(records));
	
	          resolve(records);
	        }, function (error) {
	          reject('There is no record in the storage with the id "' + id + '"');
	        });
	      });
	    }
	  };
	};
	
	module.exports = exports['default'];

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _xhrPromise = __webpack_require__(3);
	
	var _xhrPromise2 = _interopRequireDefault(_xhrPromise);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } /**
	                                                                                                                                                                                                                   * Parse API sTORAGEmANAGER adapter.
	                                                                                                                                                                                                                   * In addition to managing simple-rage values, allows to deal with unidirectional and bidirectional associations.
	                                                                                                                                                                                                                   */
	
	
	var xhr = new _xhrPromise2.default();
	
	exports.default = function (config) {
	  // set application id
	  xhr.defaults = {
	    headers: { 'X-Parse-Application-Id': config.appId }
	  };
	
	  return {
	    name: config.name,
	
	    /**
	     * Retrive a record of the given modelClass and id.
	     * One of two ids can be used: eather objectId or stdId.
	     * Parse's `objectId` has the higher priority - it will be used even if both ids are presented.
	     *
	     * @param  {mODELcLASS} modelClass
	     * @param  {[String} opt_objectId    Optional Parse `objectId`
	     * @return {Promise}
	     */
	    retrieve: function retrieve(modelClass, objectId, stdId) {
	      var className = modelClass.name;
	      var apiUrl = [config.apiUrl, className, objectId || ''].join('/');
	
	      if (!objectId) {
	        apiUrl += '?where={"' + modelClass.standardId + '":"' + (stdId || '') + '"}';
	      }
	
	      return new Promise(function (resolve, reject) {
	        xhr.GET({ url: apiUrl }).then(function (response) {
	          try {
	            var records = JSON.parse(response).results;
	            resolve(records);
	          } catch (e) {}
	        }, function (error) {
	          return reject(error);
	        });
	      });
	    },
	
	
	    /**
	     * Retrieve all records of the given class
	     * @param  {mODELcLASS} modelClass
	     * @return {Promise}
	     */
	    retrieveAll: function retrieveAll(modelClass) {
	      var modelProps = modelClass.properties;
	      var className = modelClass.name;
	      var pointers = Object.keys(modelProps).filter(function (prop) {
	        return modelProps[prop].range === 'SingleValueRef';
	      }).join(',');
	      var includeParams = pointers ? '?include=' + encodeURIComponent(pointers) : '';
	      var apiUrl = [config.apiUrl, className, includeParams].join('/');
	
	      var relation = function () {
	        var rel = [];
	
	        Object.keys(modelProps).forEach(function (prop) {
	          if (modelProps[prop].range === 'MultiValueRef') {
	            rel.push(prop, modelProps[prop]);
	          }
	        });
	
	        return rel;
	      }();
	
	      return new Promise(function (resolve, reject) {
	        xhr.GET({ url: apiUrl }).then(function (response) {
	          try {
	            (function () {
	              var records = JSON.parse(response).results;
	
	              if (relation.length) {
	                var promises = records.map(function (record) {
	                  var params = '?where=' + JSON.stringify({
	                    $relatedTo: {
	                      object: {
	                        __type: 'Pointer',
	                        className: className,
	                        objectId: record.objectId
	                      },
	                      key: relation[0]
	                    }
	                  });
	
	                  return xhr.GET({
	                    url: [config.apiUrl, relation[1].className, params].join('/')
	                  });
	                });
	
	                Promise.all(promises).then(function (responses) {
	                  responses.forEach(function (response, i) {
	                    records[i][relation[0]] = JSON.parse(response).results;
	                  });
	
	                  resolve(records);
	                }, function (error) {
	                  return reject(error);
	                });
	              } else {
	                resolve(records);
	              }
	            })();
	          } catch (e) {
	            reject(e);
	          }
	        }, function (error) {
	          return reject(error);
	        });
	      });
	    },
	
	
	    /**
	     * Add a record of the given class to the Parse storage
	     * @param {mODELcLASS} modelClass
	     * @param {Object} record
	     * @return {Promise}
	     */
	    add: function add(modelClass, record) {
	      var modelProps = modelClass.properties;
	      var className = modelClass.name;
	      var apiUrl = [config.apiUrl, className].join('/');
	      var inverseRefs = [];
	
	      Object.keys(modelProps).forEach(function (prop) {
	        if (modelProps[prop].range === 'SingleValueRef') {
	          if (record[prop].objectId) {
	            record[prop].__type = 'Pointer';
	            record[prop].className = modelProps[prop].className;
	
	            var inverseRef = modelProps[prop].inverseRef;
	
	            if (inverseRef) {
	              inverseRef.objectId = record[prop].objectId;
	              inverseRef.className = record[prop].className;
	              inverseRef.operation = 'AddRelation';
	              inverseRefs.push(inverseRef);
	            }
	          } else {
	            record[prop] = undefined;
	          }
	        } else if (modelProps[prop].range === 'MultiValueRef') {
	          if (record[prop] && record[prop].length) {
	            var relation = {
	              __op: 'AddRelation',
	              objects: record[prop].map(function (obj) {
	                return {
	                  __type: 'Pointer',
	                  className: modelProps[prop].className,
	                  objectId: obj.objectId
	                };
	              })
	            };
	
	            record[prop] = relation;
	          } else {
	            record[prop] = undefined;
	          }
	        }
	      });
	
	      return new Promise(function (resolve, reject) {
	        xhr.POST({ url: apiUrl, data: record }).then(function (result) {
	          if (inverseRefs.length) {
	            var promises = inverseRefs.map(function (ref) {
	              return xhr.PUT({
	                url: [config.apiUrl, ref.className, ref.objectId].join('/'),
	                data: _defineProperty({}, ref.name, {
	                  __op: ref.operation,
	                  objects: [{
	                    __type: 'Pointer',
	                    className: className,
	                    objectId: JSON.parse(result).objectId
	                  }]
	                })
	              });
	            });
	
	            Promise.all(promises).then(function (responses) {
	              resolve(result);
	            });
	          } else {
	            resolve(result);
	          }
	        }, function (error) {
	          return reject(error);
	        });
	      });
	    },
	
	
	    /**
	     * Update a record of the given class
	     * NOTE: currently supports only one `MultiRefValue` instance
	     * @param  {mODELcLASS} modelClass
	     * @param  {Object} record
	     * @param  {Object} newData
	     * @return {Promise}
	     */
	    update: function update(modelClass, record, newData) {
	      var modelProps = modelClass.properties;
	      var className = modelClass.name;
	      var apiUrl = [config.apiUrl, className, record.objectId].join('/');
	      var inverseRefs = [];
	      var relation = void 0;
	      var newRelation = void 0;
	      var multiRefPropName = void 0;
	
	      Object.keys(modelProps).forEach(function (prop) {
	        if (modelProps[prop].range === 'SingleValueRef') {
	          if (newData[prop].objectId) {
	            newData[prop].__type = 'Pointer';
	            newData[prop].className = modelProps[prop].className;
	
	            var inverseRef = modelProps[prop].inverseRef;
	            if (inverseRef) {
	              inverseRef.objectId = (record[prop] || newData[prop]).objectId;
	              inverseRef.className = modelProps[prop].className;
	              inverseRef.operation = 'AddRelation';
	              inverseRefs.push(inverseRef);
	            }
	          } else {
	            newData[prop] = {
	              __op: 'Delete'
	            };
	
	            if (record[prop]) {
	              var _inverseRef = modelProps[prop].inverseRef;
	              if (_inverseRef) {
	                _inverseRef.objectId = (record[prop] || newData[prop]).objectId;
	                _inverseRef.className = modelProps[prop].className;
	                _inverseRef.operation = 'RemoveRelation';
	                inverseRefs.push(_inverseRef);
	              }
	            }
	          }
	        } else if (modelProps[prop].range === 'MultiValueRef') {
	          newRelation = newData[prop];
	
	          multiRefPropName = prop;
	
	          if (newData[prop] && newData[prop].length) {
	            if (record[prop] && record[prop].length) {
	              relation = {
	                __op: 'RemoveRelation',
	                objects: record[prop].map(function (value) {
	                  return {
	                    __type: 'Pointer',
	                    className: modelProps[prop].className,
	                    objectId: value.objectId
	                  };
	                })
	              };
	
	              newData[prop] = relation;
	            } else {
	              relation = {
	                __op: 'AddRelation',
	                objects: newData[prop].map(function (value) {
	                  return {
	                    __type: 'Pointer',
	                    className: modelProps[prop].className,
	                    objectId: value.objectId
	                  };
	                })
	              };
	
	              newData[prop] = relation;
	            }
	          }
	        }
	      });
	
	      if (relation && relation.__op === 'RemoveRelation') {
	        return new Promise(function (resolve, reject) {
	          xhr.PUT({ url: apiUrl, data: newData }).then(function (result) {
	            newData[multiRefPropName] = {
	              __op: 'AddRelation',
	              objects: newRelation.map(function (value) {
	                return {
	                  __type: 'Pointer',
	                  className: modelProps[multiRefPropName].className,
	                  objectId: value.objectId
	                };
	              })
	            };
	
	            xhr.PUT({ url: apiUrl, data: newData }).then(function (result) {
	              if (inverseRefs.length) {
	                var promises = inverseRefs.map(function (ref) {
	                  return xhr.PUT({
	                    url: [config.apiUrl, ref.className, ref.objectId].join('/'),
	                    data: _defineProperty({}, ref.name, {
	                      __op: ref.operation,
	                      objects: [{
	                        __type: 'Pointer',
	                        className: className,
	                        objectId: record.objectId
	                      }]
	                    })
	                  });
	                });
	
	                Promise.all(promises).then(function (responses) {
	                  resolve(result);
	                });
	              } else {
	                resolve(result);
	              }
	            }, function (error) {
	              return reject(error);
	            });
	          }, function (error) {
	            return reject(error);
	          });
	        });
	      } else {
	        return new Promise(function (resolve, reject) {
	          xhr.PUT({ url: apiUrl, data: newData }).then(function (result) {
	            if (inverseRefs.length) {
	              var promises = inverseRefs.map(function (ref) {
	                return xhr.PUT({
	                  url: [config.apiUrl, ref.className, ref.objectId].join('/'),
	                  data: _defineProperty({}, ref.name, {
	                    __op: ref.operation,
	                    objects: [{
	                      __type: 'Pointer',
	                      className: className,
	                      objectId: record.objectId
	                    }]
	                  })
	                });
	              });
	
	              Promise.all(promises).then(function (responses) {
	                return resolve(result);
	              });
	            } else {
	              resolve(result);
	            }
	          }, function (error) {
	            return reject(error);
	          });
	        });
	      }
	    },
	
	
	    /**
	     * Destroy a rectord of the given class
	     * @param  {mODELcLASS} modelClass
	     * @param  {Object} record  Record to be deleted
	     * @return {Promise}
	     */
	    destroy: function destroy(modelClass, record) {
	      var modelProps = modelClass.properties;
	      var className = modelClass.name;
	      var apiUrl = [config.apiUrl, className, record.objectId].join('/');
	      var inverseRefNames = Object.keys(modelProps).filter(function (prop) {
	        return modelProps[prop].range === 'SingleValueRef' && modelProps[prop].inverseRef && record[prop];
	      });
	      var multiValueProp = Object.keys(modelProps).filter(function (prop) {
	        return modelProps[prop].range === 'MultiValueRef' && record[prop] && record[prop].length;
	      })[0];
	
	      if (record[multiValueProp] && record[multiValueProp].length) {
	        var relation = {
	          __op: 'RemoveRelation',
	          objects: record[multiValueProp].map(function (value) {
	            return {
	              __type: 'Pointer',
	              className: modelProps[multiValueProp].className,
	              objectId: value.objectId
	            };
	          })
	        };
	
	        record[multiValueProp] = relation;
	
	        return new Promise(function (resolve, reject) {
	          xhr.PUT({ url: apiUrl, data: record }).then(function (result) {
	            xhr.DELETE({ url: apiUrl }).then(function (result) {
	              return resolve(result);
	            }, function (error) {
	              return reject(error);
	            });
	          }, function (error) {
	            return reject(error);
	          });
	        });
	      } else {
	        return new Promise(function (resolve, reject) {
	          xhr.DELETE({ url: apiUrl }).then(function (result) {
	            if (inverseRefNames.length) {
	              var promises = inverseRefNames.map(function (refName) {
	                apiUrl = [config.apiUrl, modelProps[refName].className, record[refName].objectId].join('/');
	
	                xhr.PUT({
	                  url: apiUrl,
	                  data: {
	                    publishedBooks: {
	                      __op: 'RemoveRelation',
	                      objects: [{
	                        __type: 'Pointer',
	                        className: 'Book',
	                        objectId: record.objectId
	                      }]
	                    }
	                  }
	                });
	              });
	
	              Promise.all(promises).then(function (responses) {
	                return resolve(result);
	              });
	            } else {
	              resolve(result);
	            }
	          }, function (error) {
	            return reject(error);
	          });
	        });
	      }
	    }
	  };
	};
	
	module.exports = exports['default'];

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;'use strict';
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };
	
	(function (name, root, factory) {
	  if (typeof module !== 'undefined' && module.exports) {
	    module.exports = factory();
	  } else if (true) {
	    !(__WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	  } else {
	    root[name] = factory();
	  }
	})('XHR', undefined, function () {
	  /**
	   * Class for handling Ajax requests
	   */
	  var XHR = function XHR() {
	    this.defaults = {};
	  };
	
	  XHR.prototype.GET = GET;
	  XHR.prototype.POST = POST;
	  XHR.prototype.PUT = PUT;
	  XHR.prototype.DELETE = DELETE;
	  XHR.prototype.OPTIONS = OPTIONS;
	
	  /**
	   * Implementation of GET request method
	   * @param {object} options
	   * @return {Promise}
	   */
	  function GET(options) {
	    return request.call(this, 'get', options);
	  }
	  /**
	   * Implementation of the POST request method
	   * @param {object} options
	   * @return {Promise}
	   */
	  function POST(options) {
	    return request.call(this, 'post', options);
	  }
	  /**
	   * Implementation of the PUT request method
	   * @param {object} options
	   * @return {Promise}
	   */
	  function PUT(options) {
	    return request.call(this, 'put', options);
	  }
	  /**
	   * Implementation of the DELETE request method
	   * @param {object} options
	   * @return {Promise}
	   */
	  function DELETE(options) {
	    return request.call(this, 'delete', options);
	  }
	  /**
	   * Implementation of the OPTIONS request method
	   * @param {object} options
	   * @return {Promise}
	   */
	  function OPTIONS(options) {
	    return request.call(this, 'options', options);
	  }
	  /**
	   * Make Ajax request with given method and options
	   * @param {string} method
	   * @param {object} options
	   * @return {Promise}
	   */
	  function request(method, options) {
	    return new Promise(function (resolve, reject) {
	      var opts = {
	        method: method,
	        headers: {
	          'Content-Type': 'application/json',
	          'X-Requested-With': 'XMLHttpRequest'
	        },
	        responseFormat: 'application/json'
	      },
	          request = new window.XMLHttpRequest();
	
	      extend(opts, this.defaults);
	      extend(opts, options || {});
	
	      if (!isValidUrl(opts.url)) {
	        reject(new Error('[xhr.js]: The URL is invalid or missing!'));
	      }
	
	      if (opts.data) {
	        opts.data = JSON.stringify(opts.data);
	      }
	
	      request.open(opts.method, opts.url, true);
	
	      request.onreadystatechange = function () {
	        if (request.readyState === XMLHttpRequest.DONE) {
	          if (request.status >= 400) {
	            reject({
	              statusCode: request.status,
	              statusText: request.statusText,
	              responseText: request.responseText
	            });
	          } else {
	            resolve(request.responseText);
	          }
	        }
	      };
	
	      Object.keys(opts.headers).forEach(function (header) {
	        request.setRequestHeader(header, opts.headers[header]);
	      });
	
	      request.setRequestHeader("Accept", opts.responseFormat);
	
	      if (method === "get" || method === "delete") {
	        request.send('');
	      } else {
	        opts.requestFormat && request.setRequestHeader("Content-Type", opts.requestFormat);
	        request.send(opts.data);
	      }
	    }.bind(this));
	  }
	
	  /**
	   * Check if the given URL is in valid format
	   * @param  {string}  url
	   * @return {Boolean}
	   */
	  function isValidUrl(url) {
	    var URL_PATTERN = /\b(https?):\/\/[\-A-Za-z0-9+&@#\/%?=~_|!:,.;]*[\-A-Za-z0-9+&@#\/%=~_|??]/;
	    return url && URL_PATTERN.test(url);
	  }
	
	  /**
	   * Exten the origin object with another object properties
	   * @param  {Object} origin
	   * @param  {Object} add
	   * @return {Object} extended object
	   */
	  function extend(origin, add) {
	    if (!add || (typeof add === 'undefined' ? 'undefined' : _typeof(add)) !== 'object') return origin;
	
	    var keys = Object.keys(add);
	    var i = keys.length;
	    while (i--) {
	      if (keys[i] === 'headers') {
	        extend(origin[keys[i]], add[keys[i]]);
	      } else {
	        origin[keys[i]] = add[keys[i]];
	      }
	    }
	    return origin;
	  }
	
	  return XHR;
	});

/***/ }
/******/ ])
});
;
//# sourceMappingURL=sTORAGEmANAGER.js.map