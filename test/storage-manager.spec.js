import chai from 'chai';
import StorageManager from '../lib/sTORAGEmANAGER';

const expect = chai.expect;
let storageManager;

describe('Given StorageManager Class', () => {
  describe('when I create a new instance', () => {
    it('should throw an error when not providing the config object', () => {
      let errorMessage;
      try {
        new StorageManager();
      } catch(e) {
        errorMessage = e.toString();
      }
      expect(errorMessage).to.equal('Error: [StorageManager]: expect an adapter config object as an argument!');
    });

    it('should throw an error when the provided argument is not an object', () => {
      let errorMessage;
      try {
        new StorageManager([]);
      } catch(e) {
        errorMessage = e.toString();
      }
      expect(errorMessage).to.equal('Error: [StorageManager]: adapter config must be an object!');
    });

    it('should throw an error when the provided config object doesn\'t contain the `name` property', () => {
      let errorMessage;
      try {
        new StorageManager({});
      } catch(e) {
        errorMessage = e.toString();
      }
      expect(errorMessage).to.equal('Error: [StorageManager]: invalid adapter name!');
    });

    it('should throw an error if the adapter\'s name is invalid (not in the `adapters` list)', () => {
      let errorMessage;
      try {
        new StorageManager({name: 'FakeAdapter'});
      } catch(e) {
        errorMessage = e.toString();
      }
      expect(errorMessage).to.equal('Error: [StorageManager]: invalid adapter name!');
    });
  });
});