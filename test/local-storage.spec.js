import chai from 'chai';
import LocalStorageAdapter from '../src/local-storage.js';

const expect = chai.expect;

describe('Given LocalStorage Adapter instance', () => {
  beforeEach(() => {
    localStorage.clear();
  });

  describe('when I add a new record', () => {
    it('should update the storage', () => {
      let modelClass = {
        name: 'Book',
        standardId: 'isbn',
        instances: []
      };
      let data = {
        isbn: '123456789X',
        title: 'Best Seller'
      };

      LocalStorageAdapter.add(modelClass, data);
      expect(localStorage.getItem('Book')).to.equal(JSON.stringify(data));
    });
  });
});