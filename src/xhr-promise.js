(function (name, root, factory) {
  if (typeof module !== 'undefined' && module.exports) {
    module.exports = factory();
  } else if (typeof define === 'function' && define.amd) {
    define(factory);
  } else {
    root[name] = factory();
  }
}('XHR', this, function () {
  /**
   * Class for handling Ajax requests
   */
  var XHR = function () {
    this.defaults = {};
  };

  XHR.prototype.GET = GET;
  XHR.prototype.POST = POST;
  XHR.prototype.PUT = PUT;
  XHR.prototype.DELETE = DELETE;
  XHR.prototype.OPTIONS = OPTIONS;

  /**
   * Implementation of GET request method
   * @param {object} options
   * @return {Promise}
   */
  function GET(options) {
    return request.call(this, 'get', options);
  }
  /**
   * Implementation of the POST request method
   * @param {object} options
   * @return {Promise}
   */
  function POST(options) {
    return request.call(this, 'post', options);
  }
  /**
   * Implementation of the PUT request method
   * @param {object} options
   * @return {Promise}
   */
  function PUT(options) {
    return request.call(this, 'put', options);
  }
  /**
   * Implementation of the DELETE request method
   * @param {object} options
   * @return {Promise}
   */
  function DELETE(options) {
    return request.call(this, 'delete', options);
  }
  /**
   * Implementation of the OPTIONS request method
   * @param {object} options
   * @return {Promise}
   */
  function OPTIONS(options) {
    return request.call(this, 'options', options);
  }
  /**
   * Make Ajax request with given method and options
   * @param {string} method
   * @param {object} options
   * @return {Promise}
   */
  function request(method, options) {
    return new Promise((function (resolve, reject) {
      var opts = {
        method: method,
        headers: {
          'Content-Type': 'application/json',
          'X-Requested-With': 'XMLHttpRequest'
        },
        responseFormat: 'application/json'
      },
      request = new window.XMLHttpRequest();

      extend(opts, this.defaults);
      extend(opts, options || {});

      if (!isValidUrl(opts.url)) {
        reject(new Error('[xhr.js]: The URL is invalid or missing!'));
      }

      if (opts.data) {
        opts.data = JSON.stringify(opts.data);
      }

      request.open(opts.method, opts.url, true);

      request.onreadystatechange = function () {
        if (request.readyState === XMLHttpRequest.DONE) {
          if (request.status >= 400) {
            reject({
              statusCode: request.status,
              statusText: request.statusText,
              responseText: request.responseText
            });
          } else {
            resolve(request.responseText);
          }
        }
      }

      Object.keys(opts.headers).forEach(function (header) {
        request.setRequestHeader(header, opts.headers[header]);
      });

      request.setRequestHeader("Accept", opts.responseFormat);

      if (method === "get" || method === "delete") {
        request.send('');
      } else {
        opts.requestFormat && request.setRequestHeader("Content-Type", opts.requestFormat);
        request.send(opts.data);
      }
    }).bind(this));
  }

  /**
   * Check if the given URL is in valid format
   * @param  {string}  url
   * @return {Boolean}
   */
  function isValidUrl(url) {
    var URL_PATTERN = /\b(https?):\/\/[\-A-Za-z0-9+&@#\/%?=~_|!:,.;]*[\-A-Za-z0-9+&@#\/%=~_|??]/;
    return url && URL_PATTERN.test(url);
  }

  /**
   * Exten the origin object with another object properties
   * @param  {Object} origin
   * @param  {Object} add
   * @return {Object} extended object
   */
  function extend(origin, add) {
    if (!add || typeof add !== 'object') return origin;

    var keys = Object.keys(add);
    var i = keys.length;
    while (i--) {
      if (keys[i] === 'headers') {
        extend(origin[keys[i]], add[keys[i]])
      } else {
        origin[keys[i]] = add[keys[i]];
      }
    }
    return origin;
  }

  return XHR;
}));