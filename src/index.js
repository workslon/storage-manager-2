import LocalStorage from './local-storage';
import ParseRestApi from './parse-rest-api';

const _adapters = {
  'local-storage': LocalStorage,
  'parse-rest-api': ParseRestApi
};

export default class StorageManager {
  constructor(config) {
    if (!config) {
      throw new Error('[StorageManager]: expect an adapter config object as an argument!');
    } else if (typeof config === 'object' && !Array.isArray(config)) {
      if (_adapters[config.name]) {
        let adapter = _adapters[config.name](config);
        return Object.assign(this, adapter);
      } else {
        throw new Error('[StorageManager]: invalid adapter name!');
      }
    } else {
      throw new Error('[StorageManager]: adapter config must be an object!');
    }
  }
}
