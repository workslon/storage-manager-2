/**
 * Parse API sTORAGEmANAGER adapter.
 * In addition to managing simple-rage values, allows to deal with unidirectional and bidirectional associations.
 */
import XHR from './xhr-promise';
const xhr = new XHR();

export default (config) => {
  // set application id
  xhr.defaults = {
    headers: {'X-Parse-Application-Id': config.appId}
  };

  return {
    name: config.name,

    /**
     * Retrive a record of the given modelClass and id.
     * One of two ids can be used: eather objectId or stdId.
     * Parse's `objectId` has the higher priority - it will be used even if both ids are presented.
     *
     * @param  {mODELcLASS} modelClass
     * @param  {[String} opt_objectId    Optional Parse `objectId`
     * @return {Promise}
     */
    retrieve(modelClass, objectId, stdId) {
      let className = modelClass.name;
      let apiUrl = [config.apiUrl, className, objectId || ''].join('/');

      if (!objectId) {
        apiUrl += `?where={"${modelClass.standardId}":"${stdId || ''}"}`;
      }

      return new Promise(function (resolve, reject) {
        xhr
          .GET({url: apiUrl})
          .then(response => {
            try {
              let records = JSON.parse(response).results;
              resolve(records);
            } catch(e) {}
          }, error => reject(error));
      });
    },

    /**
     * Retrieve all records of the given class
     * @param  {mODELcLASS} modelClass
     * @return {Promise}
     */
    retrieveAll(modelClass) {
      let modelProps = modelClass.properties;
      let className = modelClass.name;
      let pointers = Object.keys(modelProps)
                      .filter(prop => modelProps[prop].range === 'SingleValueRef')
                      .join(',');
      let includeParams = pointers ?
                          `?include=${encodeURIComponent(pointers)}` : '';
      let apiUrl = [config.apiUrl, className, includeParams].join('/');

      let relation = (() => {
        let rel = [];

        Object.keys(modelProps).forEach(prop => {
          if (modelProps[prop].range === 'MultiValueRef') {
            rel.push(prop, modelProps[prop]);
          }
        });

        return rel;
      })();

      return new Promise(function (resolve, reject) {
        xhr
          .GET({url: apiUrl})
          .then(response => {
            try {
              let records = JSON.parse(response).results;

              if (relation.length) {
                let promises = records.map(record => {
                  let params = '?where=' + JSON.stringify({
                    $relatedTo: {
                      object: {
                        __type: 'Pointer',
                        className: className,
                        objectId: record.objectId
                      },
                      key: relation[0]
                    }
                  });

                  return xhr.GET({
                    url: [config.apiUrl, relation[1].className, params].join('/')
                  });
                });

                Promise.all(promises).then(responses => {
                  responses.forEach((response, i) => {
                    records[i][relation[0]] = JSON.parse(response).results;
                  });

                  resolve(records);
                }, (error) => reject(error));
              } else {
                resolve(records);
              }
            } catch(e) {
              reject(e);
            }
          }, error => reject(error));
      });
    },

    /**
     * Add a record of the given class to the Parse storage
     * @param {mODELcLASS} modelClass
     * @param {Object} record
     * @return {Promise}
     */
    add(modelClass, record) {
      let modelProps = modelClass.properties;
      let className = modelClass.name;
      let apiUrl = [config.apiUrl, className].join('/');
      let inverseRefs = [];

      Object.keys(modelProps).forEach(prop => {
        if (modelProps[prop].range === 'SingleValueRef') {
          if (record[prop].objectId) {
            record[prop].__type = 'Pointer';
            record[prop].className = modelProps[prop].className;

            let inverseRef = modelProps[prop].inverseRef;

            if (inverseRef) {
              inverseRef.objectId = record[prop].objectId;
              inverseRef.className = record[prop].className;
              inverseRef.operation = 'AddRelation';
              inverseRefs.push(inverseRef);
            }
          } else {
            record[prop] = undefined;
          }
        } else if (modelProps[prop].range === 'MultiValueRef') {
          if (record[prop] && record[prop].length) {
            let relation = {
              __op: 'AddRelation',
              objects: record[prop].map(obj => {
                return {
                  __type: 'Pointer',
                  className: modelProps[prop].className,
                  objectId: obj.objectId
                }
              })
            };

            record[prop] = relation;
          } else {
            record[prop] = undefined;
          }
        }
      });

      return new Promise((resolve, reject) => {
        xhr
          .POST({url: apiUrl, data: record})
          .then(result => {
            if (inverseRefs.length) {
              let promises = inverseRefs.map(ref => {
                return xhr.PUT({
                  url: [config.apiUrl, ref.className, ref.objectId].join('/'),
                  data: {
                    [ref.name]: {
                      __op: ref.operation,
                      objects: [{
                        __type: 'Pointer',
                        className: className,
                        objectId: JSON.parse(result).objectId
                      }]
                    }
                  }
                });
              });

              Promise.all(promises).then(responses => {
                resolve(result);
              });
            } else {
              resolve(result);
            }
          }, error => reject(error));
      });
    },

    /**
     * Update a record of the given class
     * NOTE: currently supports only one `MultiRefValue` instance
     * @param  {mODELcLASS} modelClass
     * @param  {Object} record
     * @param  {Object} newData
     * @return {Promise}
     */
    update(modelClass, record, newData) {
      let modelProps = modelClass.properties;
      let className = modelClass.name;
      let apiUrl = [config.apiUrl, className, record.objectId].join('/');
      let inverseRefs = [];
      let relation;
      let newRelation;
      let multiRefPropName;

      Object.keys(modelProps).forEach(prop => {
        if (modelProps[prop].range === 'SingleValueRef') {
          if (newData[prop].objectId) {
            newData[prop].__type = 'Pointer';
            newData[prop].className = modelProps[prop].className;

            let inverseRef = modelProps[prop].inverseRef;
            if (inverseRef) {
              inverseRef.objectId = (record[prop] || newData[prop]).objectId;
              inverseRef.className = modelProps[prop].className;
              inverseRef.operation = 'AddRelation';
              inverseRefs.push(inverseRef);
            }
          } else {
            newData[prop] = {
              __op: 'Delete'
            };

            if (record[prop]) {
              let inverseRef = modelProps[prop].inverseRef;
              if (inverseRef) {
                inverseRef.objectId = (record[prop] || newData[prop]).objectId;
                inverseRef.className = modelProps[prop].className;
                inverseRef.operation = 'RemoveRelation';
                inverseRefs.push(inverseRef);
              }
            }
          }
        } else if (modelProps[prop].range === 'MultiValueRef') {
          newRelation = newData[prop];

          multiRefPropName = prop;

          if (newData[prop] && newData[prop].length) {
            if (record[prop] && record[prop].length) {
              relation = {
                __op: 'RemoveRelation',
                objects: record[prop].map(value => {
                  return {
                    __type: 'Pointer',
                    className: modelProps[prop].className,
                    objectId: value.objectId
                  }
                })
              };

              newData[prop] = relation;
            } else {
              relation = {
                __op: 'AddRelation',
                objects: newData[prop].map(value => {
                  return {
                    __type: 'Pointer',
                    className: modelProps[prop].className,
                    objectId: value.objectId
                  }
                })
              };

              newData[prop] = relation;
            }
          }
        }
      });

      if (relation && relation.__op === 'RemoveRelation') {
        return new Promise((resolve, reject) => {
          xhr
            .PUT({url: apiUrl, data: newData})
            .then(result => {
              newData[multiRefPropName] = {
                __op: 'AddRelation',
                objects: newRelation.map(function (value) {
                  return {
                    __type: 'Pointer',
                    className: modelProps[multiRefPropName].className,
                    objectId: value.objectId
                  }
                })
              };

              xhr
                .PUT({url: apiUrl, data: newData})
                .then(result => {
                  if (inverseRefs.length) {
                    let promises = inverseRefs.map(ref => {
                      return xhr.PUT({
                        url: [config.apiUrl, ref.className, ref.objectId].join('/'),
                        data: {
                          [ref.name]: {
                            __op: ref.operation,
                            objects: [{
                              __type: 'Pointer',
                              className: className,
                              objectId: record.objectId
                            }]
                          }
                        }
                      });
                    });

                    Promise.all(promises).then(responses => {
                      resolve(result);
                    });
                  } else {
                    resolve(result);
                  }
                }, error => reject(error));
            }, error => reject(error));
        });
      } else {
        return new Promise((resolve, reject) => {
          xhr
            .PUT({url: apiUrl, data: newData})
            .then(result => {
              if (inverseRefs.length) {
                let promises = inverseRefs.map(ref => {
                  return xhr.PUT({
                    url: [config.apiUrl, ref.className, ref.objectId].join('/'),
                    data: {
                      [ref.name]: {
                        __op: ref.operation,
                        objects: [{
                          __type: 'Pointer',
                          className: className,
                          objectId: record.objectId
                        }]
                      }
                    }
                  });
                });

                Promise.all(promises).then(responses => resolve(result));
              } else {
                resolve(result);
              }
            }, error => reject(error));
        });
      }
    },

    /**
     * Destroy a rectord of the given class
     * @param  {mODELcLASS} modelClass
     * @param  {Object} record  Record to be deleted
     * @return {Promise}
     */
    destroy(modelClass, record) {
      let modelProps = modelClass.properties;
      let className = modelClass.name;
      let apiUrl = [config.apiUrl, className, record.objectId].join('/');
      let inverseRefNames = Object.keys(modelProps).filter(prop => {
        return modelProps[prop].range === 'SingleValueRef'
                && modelProps[prop].inverseRef
                && record[prop];
      });
      let multiValueProp = Object.keys(modelProps).filter(prop => {
        return modelProps[prop].range === 'MultiValueRef'
                && record[prop]
                && record[prop].length
      })[0];

      if (record[multiValueProp] && record[multiValueProp].length) {
        let relation = {
          __op: 'RemoveRelation',
          objects: record[multiValueProp].map(value => {
            return {
              __type: 'Pointer',
              className: modelProps[multiValueProp].className,
              objectId: value.objectId
            }
          })
        };

        record[multiValueProp] = relation;

        return new Promise((resolve, reject) => {
          xhr
            .PUT({url: apiUrl, data: record})
            .then(result => {
              xhr
                .DELETE({url: apiUrl})
                .then(result => resolve(result), error => reject(error));
            }, error => reject(error));
        });
      } else {
        return new Promise((resolve, reject) => {
          xhr
            .DELETE({url: apiUrl})
            .then(result => {
              if (inverseRefNames.length) {
                let promises = inverseRefNames.map(refName => {
                  apiUrl = [
                    config.apiUrl,
                    modelProps[refName].className,
                    record[refName].objectId
                  ].join('/');

                  xhr.PUT({
                    url: apiUrl,
                    data: {
                      publishedBooks: {
                        __op: 'RemoveRelation',
                        objects: [{
                          __type: 'Pointer',
                          className: 'Book',
                          objectId: record.objectId
                        }]
                      }
                    }
                  })
                });

                Promise.all(promises).then(responses => resolve(result));
              } else {
                resolve(result);
              }
            }, error => reject(error));
        });
      }
    }
  }
};
