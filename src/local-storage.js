/**
 * LocalStorage adapter
 */
export default (config) => {
  return {
    /**
     * Adapter name
     */
    name: config.name,
    /**
     * Retrieve record
     * @param  {mODELcLASS} modelClass
     * @param  {String} id
     * @return {Promise}
     */
    retrieve(modelClass, id) {
      return new Promise((resolve, reject) => {
        let records = localStorage.getItem(modelClass.name) || '{}';
        let record = JSON.parse(records)[id];

        if (record) {
          resolve(record);
        } else {
          reject(`There is no such record of the type "${modelClass.name}" with the id "${id}"`);
        }
      });
    },
    /**
     * Retrive all records
     * @param  {mODELcLASS} modelClass
     * @return {Promise}
     */
    retrieveAll(modelClass) {
      return new Promise((resolve, reject) => {
        try {
          let records = JSON.parse(localStorage.getItem(modelClass.name) || '{}');

          if (records) {
            resolve(records);
          } else {
            reject(`There is no "${modelClass.name}" object class`);
          }
        } catch(e) {
          reject(e);
        }
      });
    },
    /**
     * Add record
     * @param  {mODELcLASS} modelClass
     * @param  {Object} record
     * @return {Promise}
     */
    add(modelClass, record) {
      return new Promise((resolve, reject) => {
        let id = record[modelClass.standardId];

        if (!id) {
          throw new Error('[sTORAGEmANAGER]: The record doesn\'t contain standard id!');
        }

        this.retrieve(modelClass, id)
          .then(result => {
              reject(`The record with the given id "${id}" already exists.`);
            }, () => {
              let storageName = modelClass.name;
              let records = JSON.parse(localStorage.getItem(storageName) || '{}');

              records[id] = record;
              localStorage.setItem(storageName, JSON.stringify(records));

              modelClass.instances = modelClass.create(record); // ??? why we need to do this

              resolve(records);
            });
      });
    },
    /**
     * Update record
     * @param  {mODELcLASS} modelClass
     * @param  {String} id
     * @param  {Object} record
     * @return {Promise}
     */
    update(modelClass, id, record) {
      return new Promise((resolve, reject) => {
        try {
          let storageName = modelClass.name;
          let id = record[record[id]];
          let records = JSON.parse(localStorage.getItem(storageName) || '{}');

          records[id] = record;
          localStorage.setItem(storageName, JSON.stringify(records));

          modelClass.instances = modelClass.create(record); // ??? why we need to do this

          resolve(records);
        } catch(e) {
          reject(e);
        }
      });
    },
    /**
     * Destroy record
     * @param  {mODELcLASS} modelClass
     * @param  {String} id
     * @return {Promise}
     */
    destroy(modelClass, id) {
      return new Promise((resolve, reject) => {
        this.retrieve(modelClass, id)
          .then(result => {
            let storageName = modelClass.name;
            let records = JSON.parse(localStorage.getItem(storageName) || '{}');

            delete records[id];
            delete modelClass.instances[id];

            localStorage.setItem(storageName, JSON.stringify(records));

            resolve(records);
          }, error => {
            reject(`There is no record in the storage with the id "${id}"`);
          });
      });
    }
  }
};
